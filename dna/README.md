This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Una vez clonado el repositorio, ejecuta el comando: npm install.

Despues ejecuta el comando:
```bash
npm run dev
# or
yarn dev
```
Esto inicia un servidor de desarrollo de manera local de la aplicación.

Abrir [http://localhost:3000](http://localhost:3000) con el navegador para ver la aplicación.

Puedes acceder a la API en el siguiente end point: 
http://localhost:3000/api/cualquier_end_point...

Para obtener las estadisticas(GET): /api/stats
Para obtener la mutacion en el adn(POST): /api/mutation



## Deploy on Vercel

La API se encuentra desplegada en Vercel, para acceder: 
https://adn-tester.vercel.app/ ===> App

https://adn-tester.vercel.app/api/mutation ===> EndPoint que guarda la secuencia de adn en MongoDB y calcula las secuencias de adn.

https://adn-tester.vercel.app/api/stats ===> EndPoint que por peticion GET obtiene las estadisticas de las secuencias guardadas en MongoDB.
